<?php

namespace App\Controller;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ResultsController extends AbstractController
{
    /**
     * @Route("/", name="results")
     */
    public function index()
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://www.fis-ski.com/DB/general/results.html?sectorcode=AL&raceid=98174',
            // You can set any number of default request options.
            'timeout'  => 2.0,
            
        ]);
        $response = $client->request('GET');
        $body = $response->getBody();
        $crawler = new Crawler((string) $body);

        
       $RaceTitle = $crawler->filterXPath('//h1[@class="heading heading_l2 heading_white heading_off-sm-style"]')
       ->extract(['_text']);

       $RaceKind = $crawler->filterXPath('//div[@class="event-header__kind"]')
       ->extract(['_text']);

       $RaceType = $crawler->filterXPath('//div[@class="event-header__subtitle"]')
       ->extract(['_text']);

       $RaceDate = $crawler->filterXPath('//span[@class="date__short"]')
       ->extract(['_text']);

       $names =  $crawler->filterXPath('//div[@id="events-info-results"]/div[@class="tbody"]/a[@class="table-row"]/div[@class="g-row container"]/div[@class="g-row justify-sb"]/div[4]')
       ->extract(['_text']);

       $years =  $crawler->filterXPath('//div[@id="events-info-results"]/div[@class="tbody"]/a[@class="table-row"]/div[@class="g-row container"]/div[@class="g-row justify-sb"]/div[5]')
       ->extract(['_text']);

       $position =  $crawler->filterXPath('//div[@id="events-info-results"]/div[@class="tbody"]/a[@class="table-row"]/div[@class="g-row container"]/div[@class="g-row justify-sb"]/div[1]')
       ->extract(['_text']);

       $times =  $crawler->filterXPath('//div[@class="g-lg-2 g-md-2 justify-right blue bold hidden-sm hidden-xs"]')
       ->extract(['_text']);

       $countries =  $crawler->filterXPath('//span[@class="country__name-short"]')
       ->extract(['_text']);
    

       




        return $this->render('results/index.html.twig', [
            'controller_name' => 'ConcertsController',
            'body' => [
                'RaceTitle' => $RaceTitle,
                'RaceKind' => $RaceKind,
                'RaceType' => $RaceType,
                'RaceDate' => $RaceDate,
                'names' => $names,
                'years' => $years,
                'position' => $position,
                'times' => $times,
                'countries' => $countries
            ],
            
                    ]);
    }
}
